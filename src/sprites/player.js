import Phaser from 'phaser'
import Character from './character'

export default class extends Character {
  update() {
    this.body.velocity.y = 0;
    this.body.velocity.x = 0;

    this.moving = {
      left: false,
      right: false,
      top: false,
      down: false
    }

    if (this.keys.upKey.isDown || this.keys.wKey.isDown)
    {
        this.body.velocity.y = -100;
        this.moving.up = true;
    }
    else if (this.keys.downKey.isDown || this.keys.sKey.isDown)
    {
      this.body.velocity.y = 100;
        this.moving.down = true;
    }

    if (this.keys.leftKey.isDown || this.keys.aKey.isDown)
    {
        this.body.velocity.x = -100;
        this.moving.left = true;
    }
    else if (this.keys.rightKey.isDown || this.keys.dKey.isDown)
    {
        this.body.velocity.x = 100;
        this.moving.right = true;
    }
  }
}