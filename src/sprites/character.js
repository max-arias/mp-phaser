import Phaser from 'phaser'

export default class extends Phaser.Sprite {

  constructor ({ id, game, x, y, key, frame, isEnemy, gameSt }) {
    super(game, x, y, key, frame)

    this.game.physics.arcade.enable(this)
    this.body.collideWorldBounds = true
    this.game = game
    this.gameSt = gameSt
    this.id = id
    this.moving = {
      left: false,
      right: false,
      top: false,
      down: false
    }

    this.playerName = this.game.add.text(-50, -20, 'Player: ' + this.id, { font: "10px Arial", fill: "#ffffff" });
    this.addChild(this.playerName);
    console.log(this.playerName)

    this.keys = {
      upKey: this.game.input.keyboard.addKey(Phaser.Keyboard.UP),
      downKey: this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN),
      leftKey: this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT),
      rightKey: this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT),
      wKey: this.game.input.keyboard.addKey(Phaser.Keyboard.W),
      sKey: this.game.input.keyboard.addKey(Phaser.Keyboard.S),
      aKey: this.game.input.keyboard.addKey(Phaser.Keyboard.A),
      dKey: this.game.input.keyboard.addKey(Phaser.Keyboard.D)
    }
  }

  update () {}

}
