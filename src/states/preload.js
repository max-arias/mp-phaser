import Phaser from 'phaser'
import { centerGameObjects } from '../utils'

export default class extends Phaser.State {
  init () {
    this.game.stage.backgroundColor = "#000";
  }

  preload () {
    this.loaderBg = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBg')
    this.loaderBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loaderBar')
    centerGameObjects([this.loaderBg, this.loaderBar])

    this.load.setPreloadSprite(this.loaderBar)

    this.load.tilemap('map_1', './assets/maps/map_1.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.image('map_tiles', './assets/images/dungeon_sheet.png');
    this.load.spritesheet('map_spritesheet', './assets/images/dungeon_sheet.png', 32, 32);
    this.load.spritesheet('ui_spritesheet', './assets/images/UIpackSheet_transparent.png', 32, 32, null, 0, 4);

  }

  create () {
    this.state.start('Game')
  }

}
