/* globals __DEV__ */
import Phaser from 'phaser'
import Player from '../sprites/player'
import Enemy from '../sprites/enemy'
import Map1 from '../maps/map1'
import {Client} from '../client'
import PathFinderPlugin from '../utils/pathfinder'

export default class extends Phaser.State {

  init () {
    this.isActive = false;
    this.lastPos = {x: 0, y: 0}
    this.map = null;
    this.marker = null;
    this.pathfinder = null;

    this.game.physics.startSystem(Phaser.Physics.ARCADE);
    
    this.localClient = new Client(this, this.game);
    this.localClient.create()

    this.friendlyPlayers = this.game.add.group()
    this.enemyPlayers = this.game.add.group()
  }

  preload () {}

  create () {
    this.createMap();

    this.initPathFinding();

    // Move enemy/player groups to the top
    this.game.world.bringToTop(this.enemyPlayers);
    this.game.world.bringToTop(this.friendlyPlayers);

    // Game finished loading, tell server to create player
    this.localClient.gameLoaded();
  }

  createMap() {

    this.map = new Map1({
      game: this.game,
      key: 'map_1',
      tileWidth: 24,
      tileHeight: 24,
      width: 19,
      height: 20
    });

    this.game.world.sendToBack(this.friendlyPlayers);
    
  }

  createPlayer({id, x, y, playerType}) {
    this.player = new Player({
      id: id,
      game: this.game,
      gameSt: this,
      x: x,
      y: y,
      key: 'map_spritesheet',
      frame: playerType
    })

    this.friendlyPlayers.add(this.player);

    this.game.camera.follow(this.player);
    return this.player;
  }

  createEnemy({id, x, y, playerType}) {
    let enemy = new Enemy({
      id: id,
      game: this.game,
      x: x,
      y: y,
      key: 'map_spritesheet',
      frame: playerType
    })

    this.enemyPlayers.add(enemy);

    return enemy;
  }

  initCursor() {
    this.marker = this.game.add.graphics();
    this.marker.lineStyle(2, 0x000000, 1);
    this.marker.drawRect(0, 0, Pathfinder.tileSize, Pathfinder.tileSize);

    this.input.onDown.add(function(event){
        this.updateCursorPosition();
        this.mainPlayer.moveTo(this.marker.x, this.marker.y, function(path){

        });
    }, this);
  }

  updateCursorPosition(){
      // this.marker.x = this.walkableLayer.getTileX(this.game.input.activePointer.worldX) * 32;
      // this.marker.y = this.walkableLayer.getTileY(this.game.input.activePointer.worldY) * 32;
      // this.mainPlayer.moveTo(this.marker.x, this.marker.y, function(path){
      // });

    if (this.game.input.mousePointer.isDown)
    {
        var marker = {
          x: this.map.groundLayer.getTileX(this.game.input.activePointer.worldX) * 32,
          y: this.map.groundLayer.getTileY(this.game.input.activePointer.worldY) * 32
        }
        console.log(marker)
        // blocked = true;
        this.findPathTo(this.map.groundLayer.getTileX(marker.x), this.map.groundLayer.getTileY(marker.y));
    }
  }

  initPathFinding() {
    this.pathfinder = this.game.plugins.add(PathFinderPlugin)
    this.pathfinder.setGrid(this.map.layers[2].data, [0]);
  }

  findPathTo(tilex, tiley) {

    this.pathfinder.setCallbackFunction(function(path) {
        path = path || [];
        console.log('path', path)
        // for(var i = 0, ilen = path.length; i < ilen; i++) {
        //     map.putTile(46, path[i].x, path[i].y);
        // }
        // blocked = false;
    });

    this.pathfinder.preparePathCalculation([2, 2], [tilex,tiley]);
    this.pathfinder.calculatePath();
}

  render () {
    if (__DEV__ && this.player) {
      this.game.debug.spriteInfo(this.player, 32, 32)
    }
  }

  update () {
    game.physics.arcade.collide(this.friendlyPlayers, this.map.groundLayer);

    this.updateCursorPosition();

    if(this.player && this.player.position &&
       ((this.lastPos.x != this.player.position.x) || 
        (this.lastPos.y != this.player.position.y))
    ) {      
      // TODO: Figure out how to sync this
      // game.physics.arcade.collide(this.friendlyPlayers, this.enemyPlayers);
      this.lastPos.x = this.player.position.x
      this.lastPos.y = this.player.position.y
      this.localClient.update()
    }
  }
}
