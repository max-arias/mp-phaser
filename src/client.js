import io from 'socket.io-client'
import config from './config'

export class Client {
    constructor(gameState, gameObject) {
        this.gameSt = gameState;
        this.gameObj = gameObject;
        this.socket = null;
    }

    create() {
        this.socket = io.connect(config.hostUrl);
        let gameSt = this.gameSt;
        let gameObj = this.gameObj;
        let currentPlayer = null
        
        this.socket.on('playerSpawn', function(data) {
            currentPlayer = gameSt.createPlayer(data);
        });

        // Handle updating players
        this.socket.on('updatePlayers', function(players){
            if(currentPlayer) {
                _.each(players, function(player){
                    // Iterate enemies
                    if(player.id !== currentPlayer.id) {
                        let enemy = null;
                        // If there are no enemies, simply create one, new player
                        if(!gameSt.enemyPlayers.countLiving()) {
                            enemy = gameSt.createEnemy(player);
                        } else {
                            // If there are, find the enemy we're interating and update position
                            gameSt.enemyPlayers.forEachAlive(function(enemyPlayer) {
                                
                                if(!enemyPlayer.id) return;

                                // If we found the enemy in the enemies array, update its position
                                if(enemyPlayer.id === player.id) {
                                    enemy = enemyPlayer;
                                    enemy.position.x = player.x;
                                    enemy.position.y = player.y;
                                }
                            });

                            // If we didn't find it in the array, create it, it's new player
                            if(!enemy) {
                                enemy = gameSt.createEnemy(player);
                            }                   
                        }
                    }
                });                
            }
            
        });

        this.socket.on('removePlayer', function(disconnectedPlayerId) {
           gameSt.enemyPlayers.forEachAlive(function(enemyPlayer) {
                if(enemyPlayer.id === disconnectedPlayerId) {
                    enemyPlayer.destroy()
                }
            });
        })
    }

    update() {      
        var game = this.gameSt;  
        this.socket.emit('newPlayerPosition', {
            id: game.player.id,
            x: game.player.position.x,
            y: game.player.position.y
        });
    }

    gameLoaded() {
        this.socket.emit('gameLoaded');
    }

}
//https://bitbucket.org/m4tchb0x/survivethis/src/976f3f3ad6b74a8067ee8aebd3bf32a584ce67be?at=master