import Phaser from 'phaser'

export default class extends Phaser.Tilemap {

  constructor ({game, key, tileWidth, tileHeight, width, height}) {
    super(game, key, tileWidth, tileHeight, width, height)

    this.addTilesetImage('dungeon_sheet', 'map_spritesheet');
    this.backgroundlayer = this.createLayer('floor');
    this.groundLayer = this.createLayer('collidable');

    this.backgroundlayer.resizeWorld();
    this.groundLayer.resizeWorld();

    this.setCollisionBetween(0, 10000, true, this.groundLayer);
  }

  update () {}

}
