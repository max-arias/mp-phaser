import 'pixi'
import 'p2'
import _ from 'lodash'
import Phaser from 'phaser'

import BootState from './states/boot'
import PreLoadState from './states/preload'
import GameState from './states/game'
import config from './config'

class Game extends Phaser.Game {

  constructor () {
    const docElement = document.documentElement
    const width = docElement.clientWidth > config.gameWidth ? config.gameWidth : docElement.clientWidth
    const height = docElement.clientHeight > config.gameHeight ? config.gameHeight : docElement.clientHeight

    super(width, height, Phaser.AUTO, 'content', null)

    this.setUpStates();
  }

  setUpStates () {
    // Set up states
    this.state.add('Boot', BootState, false)
    this.state.add('PreLoad', PreLoadState, false)
    this.state.add('Game', GameState, false)
    // Boot game
    this.state.start('Boot')
  }
}

window.game = new Game()


  