var GameServer = function(io) {

  console.log('game server start')

  var gameLoop = require('node-gameloop')
    , Player = require('./player')
    , _ = require('lodash')
    , map_1_json = require('./maps/map_1')
    , maps = require('./maps')
    , currentPlayerConnectedId
    , TILE_WIDTH = 32
    , MAX_NPC_COUNT = 20
    , players = []
    , player = null
    , socket  = io
    , client  = null;

  this.onGameLoaded = function() {
    console.log(this.client.id)
    // Create player here
    var mapPosition = maps.getPossibleSpawnLocation(map_1_json, []);
    player = new Player(this.client.id, mapPosition.x, mapPosition.y);
    players.push(player);

    // Create random NPCs
    // this.spawnNPCs()

    // Tell client to spawn player
    socket.emit('playerSpawn', player);
    // Tell clients to update
    socket.emit('updatePlayers', players);
  };

  this.onNewPlayerPosition = function(data) {

      var updatedPlayer = _.find(players, function(player) { return player.id === data.id; });
      if(!updatedPlayer) return;
      updatedPlayer.x = data.x;
      updatedPlayer.y = data.y;
      socket.emit('updatePlayers', [updatedPlayer]);
  };

  this.onDisconnect = function(data) {
      // console.log('disconnected, currentPlayerConnectedId', player.id)
      if(!players.length || !player) return;

      _.remove(players, function(p) { 
        return p.id == player.id; 
      });

      socket.emit('removePlayer', player.id);
  };

  // ----------------------------

  this.init = function() {
    var self = this;
    io.on('connection', function(client) {
      self.client = client;
      self.currentPlayerConnectedId = self.client.id;

      client.on('gameLoaded', self.onGameLoaded.bind(self));
      client.on('newPlayerPosition', self.onNewPlayerPosition.bind(self));
      client.on('disconnect', self.onDisconnect.bind(self));
    });
  };

  this.spawnNPCs = function() {
      for(var i = 0; i < MAX_NPC_COUNT; i++) {
          var mapPosition = maps.getPossibleSpawnLocation(map_1_json, []);
          var player = new Player(socket.id + '-' + players.length, mapPosition.x, mapPosition.y);
          players.push(player);
      }
      // Tell clients to update
      socket.emit('updatePlayers', players);
  };

  this.init();
}

module.exports = GameServer