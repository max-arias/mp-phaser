var _ = require('lodash')

var Maps = function(){
  this.TILE_WIDTH = 32;
  this.MAP_WIDTH = 24;
}

Maps.prototype.getLayer = function(map, layerName) {
  return _.find(map.layers, function(layer) { return layer.name === layerName });
}

Maps.prototype.getPossibleSpawnLocation = function(map, placesToAvoid) {
  var collidableLayer = this.getLayer(map, 'collidable');
  var randomIndex = Math.floor(Math.random() * collidableLayer.data.length)

  console.log('randomIndex', randomIndex);
  console.log('collidableLayer.data[randomIndex]', collidableLayer.data[randomIndex]);
  console.log('placesToAvoid', placesToAvoid);

  if(!_.includes(placesToAvoid, randomIndex) && collidableLayer.data[randomIndex] === 0) {
    return this.mapIndexToCoords(randomIndex);
  } else {
    return this.getPossibleSpawnLocation(map, placesToAvoid);
  }
}

Maps.prototype.mapIndexToCoords = function(mapIndex) {
  var x = ((mapIndex % this.MAP_WIDTH) * this.MAP_WIDTH) + this.MAP_WIDTH;
  var y = (Math.floor(mapIndex / this.MAP_WIDTH) * this.MAP_WIDTH) + this.MAP_WIDTH;
  return {x: x, y: y, index: mapIndex};
}

module.exports = new Maps()