var _ = require('lodash')

var Player = function(id, x, y) {
  // Possible player types, these are the position on the spritesheet
  this.possiblePlayerTypes = [188, 189, 190, 191, 192]

  var randomPlayerType = this.possiblePlayerTypes[Math.floor(Math.random() * (this.possiblePlayerTypes.length - 1))]

  return {
    id: id,
    x: x,
    y: y,
    playerType: randomPlayerType
  };

}

// Player.prototype.something = function() {}

module.exports = Player