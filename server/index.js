var path = require('path')

// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server, { log: false });
var port = process.env.PORT || 3000;
var gameServer = require('./game/gameServer');

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
var rootDir = path.resolve(__dirname, '../');
app.use(express.static(rootDir + '/public'));

var GameServer = new gameServer(io);