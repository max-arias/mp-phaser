# Multiplayer Phaser game (No title yet)

# Made with:
 - ES6
 - Webpack
 - PhaserJs
 - SocketIO
 
# Ideas for possible games
 - Minimalistic fighting game based around elements. (Fire > wind > earth > water > Fire). Main attack ranged (one hit kill), secondary shield(based on above would block if no direct relation, reflect if winner, die if loser)
 - Don't control your player(s) directly, defend base from enemy waves, build, etc, rts-ish
 - Stealth-ish hitman/assasin game where you blend-in to NPCs

#TODO:
- [ ] Move player movement to the Server
- [ ] Move collision logic to the Server
- [ ] Allow players to write a message, display above head and on a chat/event log
- [ ] Improve Webpack/deploy scripts
- [ ] Look into MessagePack
- [ ] Improve Server structure (move player stuff to a player class, etc)
- [ ] Wait for connect to create Game

#BUGs:
- [ ] Looks like there are orphaned connected players

